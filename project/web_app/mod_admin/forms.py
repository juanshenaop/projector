from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo, Required


class CreateDatasetForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    topic_id = StringField('Topic', validators=[DataRequired()])
    source = StringField('Source', validators=[DataRequired()])
    keywords = StringField('Keywords', validators=[DataRequired()])
    description = StringField('Description', validators=[DataRequired()])
    owner = StringField('Owner', validators=[DataRequired()])
    security_level = StringField('Security_Level', validators=[DataRequired()])
    service = StringField('Service', validators=[DataRequired()])
    has_dashboard = BooleanField('has_dashboard')
    dashboard_conditions = StringField('Dashboard Conditions')
    iframe_url = StringField('iframe_url')
    has_download = BooleanField('has_download')
    download_conditions = StringField('Download Conditions')
    has_api = BooleanField('has_api')
    api_conditions = StringField('API Conditions')
    has_hyperlink = BooleanField('has_hyperlink')
    hyperlink_conditions = StringField('Hyperlink Conditions')
    hyperlink_url = StringField('Hyperlink URL')
    has_report = BooleanField('has_report')
    report_conditions = StringField('Report Conditions')
    submit = SubmitField('Create')


class UpdateDatasetForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    topic_id = StringField('Topic', validators=[DataRequired()])
    source = StringField('Source', validators=[DataRequired()])
    keywords = StringField('Keywords', validators=[DataRequired()])
    description = StringField('Description', validators=[DataRequired()])
    owner = StringField('Owner', validators=[DataRequired()])
    security_level = StringField('Security_Level', validators=[DataRequired()])
    service = StringField('Service', validators=[DataRequired()])
    has_dashboard = BooleanField('has_dashboard')
    dashboard_conditions = StringField('Dashboard Conditions')
    iframe_url = StringField('iframe_url')
    has_download = BooleanField('has_download')
    download_conditions = StringField('Download Conditions')
    has_api = BooleanField('has_api')
    api_conditions = StringField('API Conditions')
    has_hyperlink = BooleanField('has_hyperlink')
    hyperlink_conditions = StringField('Hyperlink Conditions')
    hyperlink_url = StringField('Hyperlink URL')
    has_report = BooleanField('has_report')
    report_conditions = StringField('Report Conditions')
    submit = SubmitField('Update')


class CreateTermsAndConditionsForm(FlaskForm):
    version = StringField('Version', validators=[DataRequired()])
    description = StringField('Description', validators=[DataRequired()])
    submit = SubmitField('Create')


class UpdateTermsAndConditionsForm(FlaskForm):
    version = StringField('Version', validators=[DataRequired()])
    description = StringField('Description', validators=[DataRequired()])
    submit = SubmitField('Create')
