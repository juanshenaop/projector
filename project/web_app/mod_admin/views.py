# -*- coding: utf-8 -*-
# Standard library imports
import random

# Third party imports
from flask import Flask, g, Blueprint, render_template, flash, session, redirect, url_for, request
from flask_login import LoginManager
from flask_login import login_user, logout_user, current_user, login_required

from bokeh.models import (HoverTool, FactorRange, Plot, LinearAxis, Grid, Range1d)
from bokeh.models.glyphs import VBar
from bokeh.plotting import Figure, show, output_file
from bokeh.embed import components
from bokeh.models.sources import ColumnDataSource

from sqlalchemy import exc, func

import numpy as np
import pandas as pd

# Local application imports
from project.web_app import app
import project.web_app.models as models
from project.web_app.mod_admin.forms import CreateDatasetForm, UpdateDatasetForm, CreateTermsAndConditionsForm, \
    UpdateTermsAndConditionsForm
import project.web_app.mod_core.core as core

# Define the blueprint: 'auth', set its url prefix: app.url/auth
module_administration = Blueprint('admin', __name__, url_prefix='/admin')

''' Definiciones de USUARIOS, CONJUNTOS DE INFORMACIÓN, TERMINOS Y CONDICIONES DE USO, NOTIFICACIONES Y VISITAS'''


# -- Users -- #
@module_administration.route('/users')
@login_required
def users():
    if current_user.admin is True:
        context = core.get_dashboard_info()
        users = models.User.query.filter(models.User.confirmed == 1).all()
        return render_template('admin/admin_users.html', **context, users=users)
    else:
        return redirect(url_for('catalogue.catalogue_topics'))


@module_administration.route('/users/authorization/<id_user>/')
@login_required
def user_authorization(id_user):
    if current_user.admin is True:
        user = models.User.query.filter_by(email=id_user).first()
        if user.authorized is True:
            user = models.User.query.filter_by(email=id_user).update(dict(authorized=False))
            models.db.session.commit()
        else:
            user = models.User.query.filter_by(email=id_user).update(dict(authorized=True))
            models.db.session.commit()
    return redirect(url_for('admin.users'))


@module_administration.route('/users/admin/<id_user>/')
@login_required
def user_administration(id_user):
    if current_user.admin is True:
        user = models.User.query.filter_by(email=id_user).first()
        if user.admin is True:
            user = models.User.query.filter_by(email=id_user).update(dict(admin=False))
            models.db.session.commit()
        else:
            user = models.User.query.filter_by(email=id_user).update(dict(admin=True))
            models.db.session.commit()
    return redirect(url_for('admin.users'))


@module_administration.route('/users/admin/<id_user>/<permissions>')
@login_required
def user_permissions(id_user, permissions):
    if current_user.admin is True:
        user = models.User.query.filter_by(email=id_user).first()
        if user.admin is True:
            user = models.User.query.filter_by(email=id_user).update(dict(permissions=permissions))
            models.db.session.commit()
        else:
            user = models.User.query.filter_by(email=id_user).update(dict(permissions=permissions))
            models.db.session.commit()
    return redirect(url_for('admin.users'))


# -- Datasets -- #
@module_administration.route('/datasets')
@login_required
def datasets():
    if current_user.admin is True:
        context = core.get_dashboard_info()
        subquery_ratings = models.db.session.query(models.Dataset.dataset_id, models.Dataset.title, models.Dataset.topic_id,
                                                   models.Dataset.service, models.Dataset.security_level,
                                                   func.avg(models.Rating.score).label("rating_score")).outerjoin(
            models.Rating,
            models.Dataset.dataset_id == models.Rating.dataset_id).group_by(
            models.Dataset.dataset_id, models.Dataset.title, models.Dataset.topic_id,
            models.Dataset.service, models.Dataset.security_level, models.Rating.dataset_id).subquery('ratings')

        subquery_querys = models.db.session.query(models.Dataset.dataset_id,
                                                  func.count(models.Query.dataset_id).label("query_count")).outerjoin(
            models.Query,
            models.Dataset.dataset_id == models.Query.dataset_id).group_by(
            models.Dataset.dataset_id, models.Query.dataset_id).subquery('querys')

        intermediate_query = models.db.session.query(subquery_ratings, subquery_querys).outerjoin(
            subquery_querys,
            subquery_querys.c.dataset_id == subquery_ratings.c.dataset_id).group_by(
            subquery_ratings, subquery_querys)

        datasets = intermediate_query.order_by(subquery_ratings.c.dataset_id.desc()).all()
        return render_template('admin/admin_datasets.html', **context, datasets=datasets)
    else:
        return redirect(url_for('catalogue.catalogue_topics'))


@module_administration.route('/datasets/create', methods=['GET', 'POST'])
@login_required
def create_dataset():
    if current_user.admin is True:
        context = core.get_dashboard_info()
        form = CreateDatasetForm(request.form)
        if form.validate_on_submit():
            title = request.form['title']
            topic_id = request.form['topic_id']
            source = request.form['source']
            keywords = request.form['keywords']
            description = request.form['description']
            owner = request.form['owner']
            security_level = request.form['security_level']
            service = request.form['service']
            dataset = models.Dataset(title=title, topic_id=topic_id, source=source, keywords=keywords,
                                     description=description, owner=owner, security_level=security_level,
                                     service=service,
                                     has_dashboard=form.has_dashboard.data,
                                     dashboard_conditions=form.dashboard_conditions.data,
                                     iframe_url=form.iframe_url.data, has_download=form.has_download.data,
                                     download_conditions=form.download_conditions.data, has_api=form.has_api.data,
                                     api_conditions=form.api_conditions.data, has_hyperlink=form.has_hyperlink.data,
                                     hyperlink_conditions=form.hyperlink_conditions.data,
                                     hyperlink_url=form.hyperlink_url.data,
                                     has_report=form.has_report.data, report_conditions=form.report_conditions.data)
            models.db.session.add(dataset)
            models.db.session.commit()
            return redirect(url_for('admin.datasets'))
        return render_template('admin/create_dataset.html', **context, form=form)
    else:
        return redirect(url_for('catalogue.catalogue_topics'))


@module_administration.route('/datasets/update/<id_dataset>', methods=['GET', 'POST'])
@login_required
def update_dataset(id_dataset):
    if current_user.admin is True:
        context = core.get_dashboard_info()
        dataset = models.Dataset.query.filter_by(dataset_id=id_dataset).all()
        form = UpdateDatasetForm(request.form)
        if form.validate_on_submit():
            title = request.form['title']
            topic_id = request.form['topic_id']
            source = request.form['source']
            keywords = request.form['keywords']
            description = request.form['description']
            owner = request.form['owner']
            security_level = request.form['security_level']
            service = request.form['service']
            dataset = models.Dataset.query.filter_by(dataset_id=id_dataset).update(
                dict(title=title, topic_id=topic_id, source=source, keywords=keywords,
                     description=description, owner=owner, security_level=security_level, service=service,
                     has_dashboard=form.has_dashboard.data, dashboard_conditions=form.dashboard_conditions.data,
                     iframe_url=form.iframe_url.data, has_download=form.has_download.data,
                     download_conditions=form.download_conditions.data, has_api=form.has_api.data,
                     api_conditions=form.api_conditions.data, has_hyperlink=form.has_hyperlink.data,
                     hyperlink_conditions=form.hyperlink_conditions.data, hyperlink_url=form.hyperlink_url.data,
                     has_report=form.has_report.data, report_conditions=form.report_conditions.data))
            models.db.session.commit()
            return redirect(url_for('admin.datasets'))
        return render_template('admin/update_dataset.html', **context, data=dataset, form=form)
    else:
        return redirect(url_for('catalogue.catalogue_topics'))


@module_administration.route('/datasets/delete/<id_dataset>', methods=['GET', 'POST'])
@login_required
def delete_dataset(id_dataset):
    if current_user.admin is True:
        models.Dataset.query.filter_by(id=id_dataset).delete()
        models.db.session.commit()
        return redirect(url_for('admin.datasets'))
    else:
        return redirect(url_for('catalogue.catalogue_topics'))


# -- Terms and Conditions -- #
@module_administration.route('/terms_and_conditions')
@login_required
def terms_and_conditions():
    if current_user.admin is True:
        context = core.get_dashboard_info()
        terms_and_conditions_list = models.TermsAndConditions.query.order_by(models.TermsAndConditions.id.desc()).all()
        return render_template('admin/admin_terms_and_conditions.html', **context,
                               terms_and_conditions_list=terms_and_conditions_list)
    else:
        return redirect(url_for('catalogue.catalogue_topics'))


@module_administration.route('/terms_and_conditions/create', methods=['GET', 'POST'])
@login_required
def create_terms_and_conditions():
    if current_user.admin is True:
        context = core.get_dashboard_info()
        form = CreateTermsAndConditionsForm(request.form)
        if form.validate_on_submit():
            version = request.form['version']
            description = request.form['description']
            term_and_conditions = models.TermsAndConditions(version=version, description=description)
            models.db.session.add(term_and_conditions)
            models.db.session.commit()
            return redirect(url_for('admin.datasets'))
        return render_template('admin/create_terms_and_conditions.html', **context, form=form)
    else:
        return redirect(url_for('catalogue.catalogue_topics'))


@module_administration.route('/terms_and_conditions/update/<id_terms_and_conditions>', methods=['GET', 'POST'])
@login_required
def update_terms_and_conditions(id_terms_and_conditions):
    if current_user.admin is True:
        context = core.get_dashboard_info()
        terms_and_conditions = models.TermsAndConditions.query.filter_by(id=id_terms_and_conditions).all()
        form = UpdateTermsAndConditionsForm(request.form)
        if form.validate_on_submit():
            version = request.form['version']
            description = request.form['description']
            terms_and_conditions = models.TermsAndConditions.query.filter_by(id=id_terms_and_conditions).update(
                dict(version=version, description=description))
            models.db.session.commit()
            return redirect(url_for('admin.terms_and_conditions'))
        return render_template('admin/update_terms_and_conditions.html', **context,
                               terms_and_conditions=terms_and_conditions, form=form)
    else:
        return redirect(url_for('catalogue.catalogue_topics'))


# -- Notifications -- #
@module_administration.route('/notifications', methods=['GET', 'POST'])
@login_required
def notifications():
    if current_user.admin is True:
        context = core.get_dashboard_info()
        return render_template('admin/send_notifications.html', **context)
    else:
        return redirect(url_for('catalogue.catalogue_topics'))


# -- Visits -- #
@module_administration.route('/visits')
@login_required
def visits():
    if current_user.admin is True:
        context = core.get_dashboard_info()
        df = pd.read_sql_query('SELECT * FROM visits;', models.db.session.bind)
        df.set_index('visit_on')
        df = pd.to_datetime(df['visit_on'], format='%Y-%m-%d %H:%M:%S.%f')
        count_visits_by_day = df.reset_index().set_index('visit_on').resample('1D').count()

        p = Figure(x_axis_type="datetime", plot_width=1110, plot_height=400)
        p.line('visit_on', 'index', source=count_visits_by_day)
        script, div = components(p)

        # p = Figure(title='Sine', x_axis_type="datetime", plot_width=1110, plot_height=400)
        # x = np.linspace(-10, 10, 200)
        # y = np.sin(x)
        # p.line(x=x, y=y)
        # script, div = components(p)

        return render_template('admin/show_visits.html', **context, script=script, div=div)
    else:
        return redirect(url_for('catalogue.catalogue_topics'))
