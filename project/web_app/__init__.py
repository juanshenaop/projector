# -*- coding: utf-8 -*-
from flask import Flask, render_template
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy


# Define the WSGI application object
app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})
api = Api(app)

# Define the database object which is imported by modules and controllers
db = SQLAlchemy(app)

# Configurations
app.config.from_object('config')


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('errors/404.html'), 404


# Import a module / component using its blueprint handler variable
# Importing landing modules

from project.web_app.mod_start.views import module_start as start_module
from project.web_app.mod_landing.views import module_landing as landing_module

# Importing administration modules
from project.web_app.mod_auth.views import module_authorization as auth_module
from project.web_app.mod_admin.views import module_administration as admin_module
from project.web_app.mod_projects.views import module_projects as projects_module
from project.web_app.mod_analytics.views import module_analytics as analytics_module

# Importing components modules
from project.web_app.mod_files.views import module_files as files_module
from project.web_app.mod_email.views import module_email as email_module


# Register blueprint(s)
app.register_blueprint(start_module)
app.register_blueprint(landing_module)
app.register_blueprint(auth_module)
app.register_blueprint(admin_module)
app.register_blueprint(projects_module)
app.register_blueprint(analytics_module)
app.register_blueprint(files_module)
app.register_blueprint(email_module)


# Build the database
db.create_all()
