#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, g, Blueprint, render_template, flash, session, redirect, url_for, request
from flask_login import LoginManager
from flask_login import login_user, logout_user, current_user, login_required

from sqlalchemy import exc, func
import json
import requests

from project.web_app import app
import project.web_app.models as models
from project.web_app.mod_email.token import generate_confirmation_token, confirm_token
from project.web_app.mod_email.mod_email import email_confirmation, email_reset_password
from project.web_app.mod_auth.forms import LoginForm, RegistrationForm, ForgotForm, ResetPasswordForm


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'auth.login'
login_manager.login_message = '¡Debes iniciar sesión para acceder a esta página!'

SECRET_CAPTCHA = app.config['SECRET_CAPTCHA']
SITE_KEY_CAPTCHA = app.config['SITE_KEY_CAPTCHA']


# Define the blueprint: 'auth', set its url prefix: app.url/auth
module_authorization = Blueprint('auth', __name__, url_prefix='/auth')


''' Definiciones de LOGIN, REGISTER, FORGOT, LOGOUT'''


def is_human(captcha_response):
    """ Validating recaptcha response from google server
    Returns True captcha test passed for submitted form else returns False."""

    secret = SECRET_CAPTCHA
    payload = {'response':captcha_response, 'secret':secret}
    response = requests.post("https://www.google.com/recaptcha/api/siteverify", payload)
    response_text = json.loads(response.text)
    return response_text['success']


# -- Login, Logout, Forgot -- #


@module_authorization.route('/login', methods=['GET', 'POST'])
def login():

    form = LoginForm(request.form)

    if current_user.is_authenticated and models.User.confirmed is False:
        g.user = current_user.email
        logout_user()
        return redirect(url_for('landing.index'))
    if form.validate_on_submit():
        email = request.form['email']

        user = models.User.query.filter_by(email=email).first()
        if user is None or not user.check_password(form.password.data):
            flash('¡Error! El nombre de usuario o contraseña son invalidos.')
            return redirect(url_for('auth.login'))
        if user.confirmed is False:
            flash('¡Error! Verifica en tu bandeja de correo el email de confirmación.')
        if user.confirmed is True:
            if user.authorized is False:
                flash('El administrador de la plataforma no ha aprobado la solicitud de ingreso.')
            if user.authorized is True:
                login_user(user, remember=form.remember_me.data)
                visit = models.Visit(user_id=current_user.id)
                models.db.session.add(visit)
                models.db.session.commit()
                return redirect(url_for('landing.index'))
    return render_template('auth/login.html', title='Entrar - Post[data]', form=form)


@module_authorization.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('landing.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        captcha_response = request.form['g-recaptcha-response']
        user = models.User(email=form.email.data, accept_conditions=form.accept_conditions.data)
        user.set_password(form.password.data)

        if is_human(captcha_response):

            try:
                token = generate_confirmation_token(user.email)
                try:
                    email_confirmation(email_address=user.email, token=token)
                    models.db.session.add(user)
                    models.db.session.commit()
                    flash('¡Felicitaciones! Eres un usuario registrado. Espera un correo de confirmación.')
                except:
                    flash('Algo sucedió con el registro, intentalo mas tarde.')
                return redirect(url_for('auth.login'))
            except exc.IntegrityError as e:
                flash('¡Error! Este usuario o email ya existen en nuestra plataforma.')
                return redirect(url_for('auth.register'))
                pass
        else:
            flash('¡Debes verificar el Captcha!')

    return render_template('auth/register.html', title='Registrar - Post[data]', form=form, site_key_captcha=SITE_KEY_CAPTCHA)


@module_authorization.route('/forgot', methods=['GET', 'POST'])
def forgot():
    if current_user.is_authenticated:
        return redirect(url_for('landing.index'))
    form = ForgotForm()
    if form.validate_on_submit():
        email = request.form['email']
        user = models.User.query.filter_by(email=email).first()
        if user is None:
            flash('¡Error! El nombre de usuario no existe. ¡Registrate!')
            return redirect(url_for('auth.register'))
        else:
            user = models.User(email=form.email.data)
            token = generate_confirmation_token(user.email)
            email_reset_password(email_address=user.email, token=token)
            flash('¡Ha sido enviado un correo electrónico para reestablecer su contraseña!','message')
    return render_template('auth/forgot.html', title='Restaurar Contraseña - Post[data]', form=form)


@module_authorization.route('/restore/<token>', methods=['GET', 'POST'])
def restore_email(token):
    try:
        email = confirm_token(token)
    except:
        flash('El link de restauración ha expirado.', 'danger')
    user = models.User.query.filter_by(email=email).first_or_404()
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        models.db.session.add(user)
        models.db.session.commit()
        return redirect(url_for('auth.login'))
        flash('¡En hora buena! Has reiniciado tu contraseña.', 'success')
    return render_template('auth/restore.html', title='Restaurar Contraseña - Post[data]', form=form)


@module_authorization.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('landing.index'))


@login_manager.user_loader
def load_user(id):
    return models.User.query.get(int(id))
