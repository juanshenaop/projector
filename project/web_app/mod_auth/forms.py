from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo, Required


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegistrationForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password',
                              validators=[DataRequired(message='¡Error! Las contraseñas deben ser iguales.'),
                                          EqualTo('password')])
    accept_conditions = BooleanField('Accept Conditions', validators=[
        DataRequired(message='¡Error! Debe aceptar términos y condiciones de uso.')])
    submit = SubmitField('Register')


class ForgotForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Reset')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password',
                              validators=[DataRequired(message='¡Error! Las contraseñas deben ser iguales.'),
                                          EqualTo('password')])
    submit = SubmitField('Reset')


# class LoginForm(FlaskForm):
#     email = TextField('Email Address', [Email(), Required(message='Forgot your email address?')])
#     password = PasswordField('Password', [Required(message='Must provide a password. ;-)')])
