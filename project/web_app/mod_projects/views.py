# -*- coding: utf-8 -*-
# Standard library imports

# Third party imports
from flask import Flask, g, Blueprint, render_template, flash, session, redirect, url_for, request
from flask_login import LoginManager
from flask_login import login_user, logout_user, current_user, login_required

from bokeh.models import (HoverTool, FactorRange, Plot, LinearAxis, Grid, Range1d)
from bokeh.models.glyphs import VBar
from bokeh.plotting import Figure, show, output_file
from bokeh.embed import components
from bokeh.models.sources import ColumnDataSource
from bokeh.io import show, output_file
from bokeh.models import Plot, Range1d, MultiLine, Circle, HoverTool, TapTool, BoxSelectTool
from bokeh.models.graphs import from_networkx, NodesAndLinkedEdges, EdgesAndLinkedNodes
from bokeh.palettes import Spectral4

import numpy as np
import pandas as pd

import networkx as nx

from sqlalchemy import exc, func

# Local application imports
import project.web_app.models as models
import project.web_app.mod_core.core as core

# Define the blueprint: 'landing', set its url prefix: app.url/landing
module_projects = Blueprint('projects', __name__, url_prefix='/projects')

''' Definitions CARDS, TABLE '''


@module_projects.route('/cards')
def cards():
    context = core.get_landing_info()

    return render_template('projects/cards.html', **context)


@module_projects.route('/faqs')
def faqs():
    context = core.get_landing_info()

    return render_template('projects/faqs.html', **context)


@module_projects.route('/table')
def table():
    context = core.get_landing_info()

    return render_template('projects/table.html', **context)


@module_projects.route('/create_project')
def create_project():
    context = core.get_landing_info()

    return render_template('projects/project_form.html', **context)


@module_projects.route('/matrix')
def matrix():
    context = core.get_landing_info()

    source = ColumnDataSource(data=dict(
        x=[1, 2, 3, 4, 5],
        y=[2, 5, 8, 2, 7],
        size=[50, 20, 45, 25, 32],
        color=['navy', 'red', 'green', 'blue', 'cian'],
        nombres=['Interconexión', 'RITEL', 'Grandes Impositores', 'Reportes de Información', 'Calidad Móvil']
    ))

    TOOLTIPS = """
        <div>
            <div>
                <span style="font-size: 17px; font-weight: bold;">Orden</span>
                <span style="font-size: 15px; color: #966;">[$index]</span>
            </div>
            <div>
                <span style="font-size: 15px;">@nombres</span>
                <span style="font-size: 10px; color: #696;">($x, $y)</span>
            </div>
        </div>
    """

    p = Figure(plot_width=1110, plot_height=400, tooltips=TOOLTIPS)
    # x = np.linspace(-10, 10, 200)
    # y = np.sin(x)

    p.circle('x', 'y', size='size', color='color', alpha=0.5, source=source)
    script, div = components(p)

    return render_template('projects/matrix.html', **context, script=script, div=div)


@module_projects.route('/knowledge_graph')
def knowledge_graph():
    context = core.get_landing_info()

    G = nx.karate_club_graph()

    p = Figure(plot_width=1100, plot_height=700, x_range=Range1d(-1.1,1.1), y_range=Range1d(-1.1,1.1))

    SAME_CLUB_COLOR, DIFFERENT_CLUB_COLOR = "black", "red"
    edge_attrs = {}

    for start_node, end_node, _ in G.edges(data=True):
        edge_color = SAME_CLUB_COLOR if G.nodes[start_node]["club"] == G.nodes[end_node][
            "club"] else DIFFERENT_CLUB_COLOR
        edge_attrs[(start_node, end_node)] = edge_color

    nx.set_edge_attributes(G, edge_attrs, "edge_color")

    node_hover_tool = HoverTool(tooltips=[("index", "@index"), ("club", "@club")])
    p.add_tools(node_hover_tool)

    graph_renderer = from_networkx(G, nx.spring_layout, scale=1, center=(0, 0))

    graph_renderer.node_renderer.glyph = Circle(size=15, fill_color=Spectral4[0])
    graph_renderer.edge_renderer.glyph = MultiLine(line_color="edge_color", line_alpha=0.8, line_width=1)

    p.renderers.append(graph_renderer)

    script, div = components(p)

    return render_template('projects/relations.html', **context, script=script, div=div)