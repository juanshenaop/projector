# -*- coding: utf-8 -*-
# Standard library imports
import os, glob
import datetime

# Third party imports
from flask import Flask, g, Blueprint, render_template, flash, session, redirect, url_for, request, send_from_directory, \
    send_file
from flask_login import LoginManager
from flask_login import login_user, logout_user, current_user, login_required

from werkzeug.utils import secure_filename

from sqlalchemy import exc, func

# Local application imports
from project.web_app import app
import project.web_app.models as models
import project.web_app.mod_core.core as core


# Define the blueprint: 'files', set its url prefix: app.url/files
module_files = Blueprint('files', __name__, url_prefix='/files')


''' Definitions: FILES'''

UPLOAD_DATA_FOLDER = app.config['UPLOAD_DATA_FOLDER']
UPLOAD_REPORTS_FOLDER = app.config['UPLOAD_REPORTS_FOLDER']
ALLOWED_DOCUMENT_EXTENSIONS = set(['docx'])


@module_files.route('/upload/', methods=['GET', 'POST'])
@login_required
def upload():
    return render_template('files/files.html')


@module_files.route('/upload_document/', methods=['GET', 'POST'])
@login_required
def upload_document():
    data_directory = os.path.abspath(os.path.join(__file__, "../../documents/compared"))
    file = glob.glob(data_directory + '/**/' + '{}.docx'.format(datetime.datetime.now()), recursive=True)
    if request.method == 'POST':
        file = request.files['file']
        if file.filename == '':
            flash('Archivo no seleccionado')
            return redirect(url_for('files.upload_document'))
        else:
            file.filename = '{}.docx'.format(datetime.datetime.now())
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOAD_DATA_FOLDER, filename))
            return redirect(url_for('analytics.comparator'))
    return render_template('files/upload_dataset.html', file=file)


# @module_files.route('/upload/<id_dataset>', methods=['GET', 'POST'])
# @login_required
# def upload(id_dataset):
#     context = core.get_dashboard_info()
#
#     return render_template('files/files.html', **context, id_dataset=id_dataset)
#
#
# @module_files.route('/download_data/<id_dataset>', methods=['GET', 'POST'])
# @login_required
# def download_data(id_dataset):
#     try:
#         data_directory = os.path.abspath(os.path.join(__file__, "../../../files/data"))
#         file = glob.glob(data_directory + '/**/' + '{}.xlsx'.format(id_dataset), recursive=True)
#         dataset = models.Dataset.query.filter_by(dataset_id=id_dataset).first()
#         query = models.Query(user_id=current_user.id, dataset_id=id_dataset, query_type=2)
#         models.db.session.add(query)
#         models.db.session.commit()
#         return send_file(file[0], attachment_filename="{}.xlsx".format(dataset.title), as_attachment=True)
#     except Exception as e:
#         return str(e)
#
#
# @module_files.route('/upload_data/<id_dataset>', methods=['GET', 'POST'])
# @login_required
# def upload_data(id_dataset):
#     if current_user.admin is True:
#         context = core.get_dashboard_info()
#         data_directory = os.path.abspath(os.path.join(__file__, "../../../files/data"))
#         file = glob.glob(data_directory + '/**/' + '{}.xlsx'.format(id_dataset), recursive=True)
#         if request.method == 'POST':
#             file = request.files['file']
#             if file.filename == '':
#                 flash('Archivo no seleccionado')
#                 return redirect(url_for('files.upload_data'))
#             else:
#                 file.filename = '{}.xlsx'.format(id_dataset)
#                 filename = secure_filename(file.filename)
#                 file.save(os.path.join(UPLOAD_DATA_FOLDER, filename))
#                 return redirect(url_for('admin.datasets'))
#         return render_template('files/upload_dataset.html', **context, file=file)
#     else:
#         return redirect(url_for('catalogue.catalogue_topics'))
#
#
# @module_files.route('/download_report/<id_dataset>', methods=['GET', 'POST'])
# @login_required
# def download_report(id_dataset):
#     try:
#         data_directory = os.path.abspath(os.path.join(__file__, "../../../files/reports"))
#         file = glob.glob(data_directory + '/**/' + '{}.pdf'.format(id_dataset), recursive=True)
#         dataset = models.Dataset.query.filter_by(dataset_id=id_dataset).first()
#         query = models.Query(user_id=current_user.id, dataset_id=id_dataset, query_type=4)
#         models.db.session.add(query)
#         models.db.session.commit()
#         return send_file(file[0], attachment_filename="{}.pdf".format(dataset.title), as_attachment=True)
#     except Exception as e:
#         return str(e)
#
#
# @module_files.route('/upload_report/<id_dataset>', methods=['GET', 'POST'])
# @login_required
# def upload_report(id_dataset):
#     if current_user.admin is True:
#         context = core.get_dashboard_info()
#         data_directory = os.path.abspath(os.path.join(__file__, "../../../files/reports"))
#         file = glob.glob(data_directory + '/**/' + '{}.pdf'.format(id_dataset), recursive=True)
#         if request.method == 'POST':
#             file = request.files['file']
#             if file.filename == '':
#                 flash('Archivo no seleccionado')
#                 return redirect(url_for('files.upload_data'))
#             else:
#                 file.filename = '{}.pdf'.format(id_dataset)
#                 filename = secure_filename(file.filename)
#                 file.save(os.path.join(UPLOAD_REPORTS_FOLDER, filename))
#                 return redirect(url_for('admin.datasets'))
#         return render_template('files/upload_report.html', **context, file=file)
#     else:
#         return redirect(url_for('catalogue.catalogue_topics'))
#
#
# @module_files.route('/visit/<id_dataset>', methods=['GET', 'POST'])
# @login_required
# def visit(id_dataset):
#     dataset = models.Dataset.query.filter_by(dataset_id=id_dataset).all()
#     hyperlink = dataset[0].hyperlink_url
#     query = models.Query(user_id=current_user.id, dataset_id=id_dataset, query_type=3)
#     models.db.session.add(query)
#     models.db.session.commit()
#     return redirect(url_for('catalogue.catalogue_topics', id_topic=dataset[0].topic_id))
#
#
# @module_files.route('/compare', methods=['GET', 'POST'])
# @login_required
# def compare():
#     #context = core.get_dashboard_info()
#
#     return render_template('files/files.html')
#
#
# @module_files.route('/upload_document/', methods=['GET', 'POST'])
# @login_required
# def upload_document():
#     id_dataset = 1
#     if current_user.admin is True:
#         context = core.get_dashboard_info()
#         data_directory = os.path.abspath(os.path.join(__file__, "../../../files/data"))
#         file = glob.glob(data_directory + '/**/' + '{}.xlsx'.format(id_dataset), recursive=True)
#         if request.method == 'POST':
#             file = request.files['file']
#             if file.filename == '':
#                 flash('Archivo no seleccionado')
#                 return redirect(url_for('files.upload_data'))
#             else:
#                 file.filename = '{}.xlsx'.format(id_dataset)
#                 filename = secure_filename(file.filename)
#                 file.save(os.path.join(UPLOAD_DATA_FOLDER, filename))
#                 return redirect(url_for('admin.datasets'))
#         return render_template('files/upload_dataset.html', **context, file=file)
#     else:
#         return redirect(url_for('catalogue.catalogue_topics'))