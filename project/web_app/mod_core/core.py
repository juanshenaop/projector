#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime

from flask import g
from flask_login import current_user

from sqlalchemy import func

import project.web_app.models as models


# -- General Functions --- #
def get_dashboard_info():
    count_users = models.db.session.query(models.User).count()
    count_visits = models.db.session.query(models.Visit).count()
    count_datasets = models.db.session.query(models.Dataset).count()
    topics = models.Topic.query.order_by(models.Topic.id.asc()).all()

    if current_user.is_authenticated:
        g.user = current_user.email
        user = models.User.query.filter_by(email=current_user.email).first()
        visits = models.Visit.query.order_by(models.Visit.visit_on.desc()).limit(3).all()
        visit = visits[0].visit_on
        g.visit = datetime.datetime.strftime(visit, '%m/%d/%Y %H:%M:%S')
        context = {
            'count_users': count_users,
            'count_visits': count_visits,
            'count_datasets': count_datasets,
            'topics': topics,
            'user_name': user.name,
            'user_admin': user.admin
        }

    return context


def get_landing_info():
    #count_users = models.db.session.query(models.User).count()
    #count_visits = models.db.session.query(models.Visit).count()
    #count_datasets = models.db.session.query(func.count(models.Dataset.dataset_id)).filter(models.Dataset.security_level == 4).scalar()

    count_users = 1
    count_visits = 1
    count_datasets = 1

    context = {'count_users': count_users,
               'count_visits': count_visits,
               'count_datasets': count_datasets
               }

    return context
