# -*- coding: utf-8 -*-
# Standard library imports
import os
import smtplib
import codecs

# Third party imports
import jinja2

# Local application imports
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage
from email.mime.text import MIMEText

from project.web_app import app


SERVER_DIR = app.config['SERVER_DIR']
SERVER_EMAIL = app.config['SERVER_EMAIL']
SMTP_EMAIL = app.config['SMTP_EMAIL']
PASS_EMAIL = app.config['PASS_EMAIL']


with codecs.open('web_app/templates/email/email_confirmation.html', "r", "utf-8") as template:
    html_email_confirmation = str(template.read())

with codecs.open('web_app/templates/email/password_reset.html', "r", "utf-8") as template:
    html_password_reset = str(template.read())


def email_confirmation(email_address, token):
    action = 'email/confirm/'
    if SERVER_EMAIL == 'OUTLOOK':
        # Create server object with SSL option
        server = smtplib.SMTP('smtp.office365.com', 587)
        server.ehlo()
        server.starttls()
        server.ehlo()

        # Perform operations via server
        server.login(SMTP_EMAIL, PASS_EMAIL)  # Define to/from
        sender = SMTP_EMAIL
        link_confirmation = SERVER_DIR + action + token
        recipient = email_address

        # Send Email without Style
        # html_raw = "<html> Gracias por registrarse en la Voz del Usuario <br>  Activa tu cuenta dando clíc: {}</html>"
        # html = html_raw.format(link_confirmation)

        # Send Email with Style
        html = jinja2.Template(html_email_confirmation).render(link_confirmation=link_confirmation)

        # Create message
        file = './web_app/static/img/email_hero.png'
        name = 'email_hero.png'

        msg = MIMEMultipart()
        msg["To"] = recipient
        msg["From"] = str(("Post[data] <{}>".format(sender)))
        msg["Subject"] = "Confirmación de Cuenta - Post[data]"
        msg['Content-Type'] = "text/html; charset=utf-8"

        msgText = MIMEText(html, 'html')
        msg.attach(msgText)  # Added, and edited the previous line

        fp = open(file, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', '<{}>'.format(name))
        msg.attach(img)

        server.sendmail(sender, recipient, msg.as_string())

        server.quit()


def email_reset_password(email_address, token):
    action = 'auth/restore/'
    if SERVER_EMAIL == 'OUTLOOK':
        # Create server object with SSL option
        server = smtplib.SMTP('smtp.office365.com', 587)
        server.ehlo()
        server.starttls()
        server.ehlo()

        # Perform operations via server
        server.login(SMTP_EMAIL, PASS_EMAIL)  # Define to/from
        sender = SMTP_EMAIL
        link_reset_password = SERVER_DIR + action + token
        recipient = email_address

        # Send Email without Style
        # html_raw = """<html> <img src="cid:email_hero.png"> Reinicio de Contraseña de POSTData <br>  Reinicia tu cuenta dando clíc: {}</html>"""
        # html = html_raw.format(link_reset_password)

        # Send Email with Style
        html = jinja2.Template(html_password_reset).render(link_reset_password=link_reset_password)

        # Create message
        file = './web_app/static/img/email_hero.png'
        name = 'email_hero.png'

        msg = MIMEMultipart()
        msg["To"] = recipient
        msg["From"] = str(("Post[data] <{}>".format(sender)))
        msg["Subject"] = "Reinicio de Contraseña - Post[data]"
        msg['Content-Type'] = "text/html; charset=utf-8"

        msgText = MIMEText(html, 'html')
        msg.attach(msgText)  # Added, and edited the previous line

        fp = open(file, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', '<{}>'.format(name))
        msg.attach(img)

        server.sendmail(sender, recipient, msg.as_string())

        server.quit()


def email_sender(email_address):
    action = 'auth/restore/'
    if SERVER_EMAIL == 'OUTLOOK':
        # Create server object with SSL option
        server = smtplib.SMTP('smtp.office365.com', 587)
        server.ehlo()
        server.starttls()
        server.ehlo()

        # Perform operations via server
        server.login(SMTP_EMAIL, PASS_EMAIL)  # Define to/from
        data_directory = os.path.abspath(os.path.join(__file__))
        sender = SMTP_EMAIL
        link_reset_password = SERVER_DIR + action
        recipient = email_address

        # Send Email without Style
        html_raw = """<html> <img src="cid:email_hero.png"> Reinicio de Contraseña de POSTData <br>  Reinicia tu cuenta dando clíc: {}</html>"""
        html = html_raw.format(link_reset_password)

        # Send Email with Style
        # html = jinja2.Template(html_raw).render(link_confirmation=link_confirmation)

        # Create message
        file = './web_app/static/img/email_hero.png'
        name = 'email_hero.png'

        msg = MIMEMultipart()
        msg["To"] = recipient
        msg["From"] = str(("Post[data] <{}>".format(sender)))
        msg["Subject"] = "Mensaje de Prueba - Post[data]"
        msg['Content-Type'] = "text/html; charset=utf-8"

        msgText = MIMEText(html, 'html')
        msg.attach(msgText)  # Added, and edited the previous line

        fp = open(file, 'rb')
        img = MIMEImage(fp.read())
        fp.close()
        img.add_header('Content-ID', '<{}>'.format(name))
        msg.attach(img)

        server.sendmail(sender, recipient, msg.as_string())

        server.quit()


if __name__ == "__main__":
    link_confirmation = "http://localhost:8080/x778sHuh26Wx"
    email_address = 'juan.henao@crcom.gov.co'
    email_confirmation(email_address=email_address, link_confirmation=link_confirmation)
