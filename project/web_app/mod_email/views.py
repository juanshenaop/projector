# -*- coding: utf-8 -*-
# Standard library imports
from datetime import datetime

# Third party imports
from flask import Blueprint, flash, redirect, url_for

# Local application imports
import project.web_app.models as models
from project.web_app.mod_email.mod_email import email_sender
from .token import confirm_token


module_email = Blueprint('email', __name__, url_prefix='/email')


''' Definitions CONFIRM_EMAIL, SEND_EMAIL_TEST'''


@module_email.route('/confirm/<token>')
def confirm_email(token):
    try:
        email = confirm_token(token)
    except:
        flash('El link de confirmación ha expirado.', 'danger')
    user = models.User.query.filter_by(email=email).first_or_404()
    if user.confirmed:
        flash('Su cuenta es ya ha sido confirmada.', 'success')
    else:
        user.confirmed = True
        user.confirmed_on = datetime.now()
        models.db.session.add(user)
        models.db.session.commit()
        flash('¡Gracias! Has confirmado tu cuenta.', 'success')
    return redirect(url_for('auth.login'))


@module_email.route('/test/<email_address>')
def send_email_test(email_address):
    email = email_sender(email_address)
    return "We just send you a Message"
