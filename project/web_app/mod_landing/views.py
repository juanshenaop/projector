# -*- coding: utf-8 -*-
# Standard library imports

# Third party imports
from flask import Flask, g, Blueprint, render_template, flash, session, redirect, url_for, request
from flask_login import LoginManager
from flask_login import login_user, logout_user, current_user, login_required
from sqlalchemy import exc, func

# Local application imports
import project.web_app.models as models
import project.web_app.mod_core.core as core


# Define the blueprint: 'landing', set its url prefix: app.url/landing
module_landing = Blueprint('landing', __name__, url_prefix='/landing')


''' Definiciones INDEX, RESEARCHES, USERS, OPERATORS, TERMS AND CONDITIONS '''


@module_landing.route('/index')
def index():
    context = core.get_landing_info()
    print(context)

    return render_template('landing/index.html', **context)
