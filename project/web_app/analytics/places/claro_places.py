# encoding=utf8
import sys

reload(sys)
sys.setdefaultencoding('utf8')

import urllib2
import requests
import json
import csv
import time
from bs4 import BeautifulSoup


# http://applications.claro.com.co/miniapps/CAVs/CentrosAtencion/getAllCentros?CiudadID=51
# request = urllib2.Request("http://applications.claro.com.co/miniapps/CAVs/CentrosAtencion/getAllCentros?CiudadID=51")

#lista_departamentos = [91, 5, 81, 8, 103, 13, 15, 17, 18, 85, 19, 20, 27, 23, 100, 94, 95, 41, 44, 47, 50, 52, 54, 86,
#                       63, 66, 102, 68, 70, 73, 76, 97, 99]

lista_departamentos = [91, 5, 81]

# for departamento in lista_departamentos:
#
#     request_depto = requests.get(
#         'http://applications.claro.com.co/miniapps/CAVs/CentrosAtencion/SelecCiudad?DepartamentoID={}'.format(departamento))
#     time.sleep(1)
#
#     #print request_depto.content
#
#     soup = BeautifulSoup(request_depto.content)
#     print soup.find_all('option')


for ciudad in range(1, 2000):

    try:
        request_ciudad = requests.get('http://applications.claro.com.co/miniapps/CAVs/CentrosAtencion/getAllCentros?CiudadID={}'.format(ciudad))
        d = json.loads(request_ciudad.content)
        time.sleep(1)
        print d
        f = csv.writer(open('test.csv', 'a'))

        for place in d:
            f.writerow([ciudad, place['nombre'], place['horario'], place['direccion'], place['tipoCAVs']])

    except ValueError:
        pass
