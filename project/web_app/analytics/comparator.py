# -*- coding: utf-8 -*-
# Standard library imports
import os, glob

# Third party imports
import numpy as np
import nltk
from nltk.tokenize import word_tokenize, sent_tokenize
import gensim
import docx
import PyPDF2


# Local application imports


def get_word_paragraphs(filename):
    doc = docx.Document(filename)
    full_text = []
    for para in doc.paragraphs:
        full_text.append(para.text)
    return full_text


def get_word_text(filename):
    doc = docx.Document(filename)
    full_text = []
    for para in doc.paragraphs:
        full_text.append(para.text)
    return '\n'.join(full_text)


def load_document_in_paragraphs(file):
    paragraphs = get_word_paragraphs(file)
    gen_docs = [[w.lower() for w in word_tokenize(text)] for text in paragraphs]
    return gen_docs


def load_document_in_text(file):
    full_text = get_word_text(file)
    gen_docs = [[w.lower() for w in word_tokenize(full_text)]]
    return gen_docs


def get_similarity(main_text, query_text):
    """Initial Text"""
    gen_docs = [[w.lower() for w in word_tokenize(text)] for text in main_text]
    dictionary = gensim.corpora.Dictionary(gen_docs)
    corpus = [dictionary.doc2bow(gen_doc) for gen_doc in gen_docs]
    tf_idf = gensim.models.TfidfModel(corpus)
    sims = gensim.similarities.Similarity('./', tf_idf[corpus], num_features=len(dictionary))

    query_doc = [w.lower() for w in word_tokenize(query_text)]
    query_doc_bow = dictionary.doc2bow(query_doc)
    query_doc_tf_idf = tf_idf[query_doc_bow]

    results = sims[query_doc_tf_idf]

    return results


def compare_documents(base_document, query_document):
    list_of_paragraphs = []
    for query_paragraph in query_document:
        sim_val = get_similarity(base_document, query_paragraph)
        max_element = np.amax(sim_val)
        index = np.where(sim_val == np.amax(sim_val))

        dict_text = {}

        if max_element >= 0.6:
            dict_text['query'] = query_paragraph
            dict_text['base'] = base_document[index[0][0]]
            dict_text['val'] = max_element
        else:
            dict_text['query'] = query_paragraph
            dict_text['base'] = ''
            dict_text['val'] = 0

        list_of_paragraphs.append(dict_text)
    return list_of_paragraphs


if __name__ == "__main__":
    print("MAIN")
    # document_one = get_word_text(r'D:\Development\Sapiens\project\documents\jurassic_world.docx')
    # document_two = get_word_text(r'D:\Development\Sapiens\project\documents\desventaja.docx')
    # document_three = get_word_text(r'D:\Development\Sapiens\project\documents\deseos.docx')
    #
    # raw_documents = [document_one, document_two, document_three]
    # query_document = [document_three]
    # print(get_similarity(raw_documents, query_document[0]))
    #
    # base_paragraphs_list = get_word_paragraphs(r'D:\Development\Sapiens\project\documents\jurassic_world.docx')
    # base_document = list(filter(None, base_paragraphs_list))
    #
    # query_paragraphs_list = get_word_paragraphs(r'D:\Development\Sapiens\project\documents\desventaja.docx')
    # query_document = list(filter(None, query_paragraphs_list))
    #
    # print(compare_documents(base_document, query_document))

    # list_of_paragraphs = []
    #
    # for query_paragraph in query_document:
    #     sim_val = get_similarity(base_document, query_paragraph)
    #     max_element = np.amax(sim_val)
    #     index = np.where(sim_val == np.amax(sim_val))
    #
    #     dict_text = {}
    #
    #     if max_element >= 0.6:
    #         dict_text['query'] = query_paragraph
    #         dict_text['base'] = base_document[index[0][0]]
    #         dict_text['val'] = max_element
    #     else:
    #         dict_text['query'] = query_paragraph
    #         dict_text['base'] = ''
    #         dict_text['val'] = ''
    #
    #     list_of_paragraphs.append(dict_text)
    #
    # print(list_of_paragraphs)

