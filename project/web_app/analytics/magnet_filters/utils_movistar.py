# -*- coding: utf-8 -*-
# Standard library imports
import re

# Third party imports

# Local application imports


""" Componente Extractor de Valores - MOVISTAR """


def get_price(text):
    pattern = re.compile(r"([0-9]{1,9}\.[0-9]{2,9})")
    price = pattern.findall(str(text))
    price = list(filter(None, price))
    return price[0]


def get_data(text):
    pattern = re.compile(r"([0-9]{0,9}.[0-9]{0,9}\wB)")
    data = pattern.search(str(text))
    if data:
        data = data.group(0)
        value = data.replace(" ", "")
    else:
        value = None
    return value


def get_minutes(text):
    def get_minutes_string(text):
        pattern = re.compile(r"> .*?or")
        minutes_string = pattern.findall(str(text))
        minutes_string = list(filter(None, minutes_string))
        return minutes_string
    pattern = re.compile(r"([^{]+Min[^}])")
    minutes = pattern.findall(str(text))
    minutes = list(filter(None, minutes))
    return minutes


def get_sms(text):
    def get_sms_from_string(text):
        pattern = re.compile(r"([0-9]{0,9})")
        sms = pattern.findall(str(text))
        sms = list(filter(None, sms))
        try:
            if len(sms) == 0:
                return "Ilimitado"
            elif sms is not None:
                return sms[0]
        except IndexError:
            pass
    pattern = re.compile(r"([a-zA-Z0-9\s:]*SMS[a-zA-Z0-9\s:]*)")
    if pattern.findall(str(text)) is not None:
        sms_string = pattern.findall(str(text))
        sms_string = list(filter(None, sms_string))
        if len(sms_string) == 0:
            return "No Tiene SMS"
        elif len(sms_string) > 0:
            return get_sms_from_string(sms_string)


def get_zero_rating(text):
    pattern = re.compile(r"(facebook)|(WhatsApp)|(Twitter)")
    socialnetworks_string = pattern.findall(str(text))
    socialnetworks_string = list(filter(None, socialnetworks_string))
    return socialnetworks_string


def get_apps(text):
    pattern = re.compile(r"(Claro_Musica_y_Claro_Video)")
    apps_string = pattern.findall(str(text))
    apps_string = list(filter(None, apps_string))
    return apps_string


def get_life_span(text):
    pattern = re.compile(r"(mes)|(quincenales)|(semana)")
    if pattern.findall(str(text)) is not None:
        life_span_string = pattern.findall(str(text))
        life_span_string = list(filter(None, life_span_string))
        if len(life_span_string[0][0]) > 0:
            return "30 Días"
        elif len(life_span_string[0][1]) > 0:
            return "15 Días"
        elif len(life_span_string[0][2]) > 0:
            return "7 Días"
