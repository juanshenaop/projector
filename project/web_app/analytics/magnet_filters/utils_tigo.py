# -*- coding: utf-8 -*-
# Standard library imports
import re

# Third party imports

# Local application imports


""" Componente Extractor de Valores - TIGO-UNE """


def get_price(text):
    pattern = re.compile(r"([0-9]{1,9}\.[0-9]{1,9})")
    price = pattern.search(str(text))
    price = price.group(0)
    return price


def get_data(text):
    pattern = re.compile(r"([0-9]{0,9}.[0-9]{0,9}\wB)")
    data = pattern.search(str(text))
    if data:
        data = data.group(0)
        value = data.replace(" ", "")
    else:
        value = None
    return value


def get_minutes(text):

    def get_minutes_from_string(text):
        pattern = re.compile(r"([0-9]{0,9})")

        if pattern is not None:
            minutes = pattern.findall(str(text))
            minutes = list(filter(None, minutes))
            return minutes

    pattern = re.compile(r"([^>]+Ilimitados)|([0-9]* Min[a-zA-Z ]*)")

    if pattern.findall(str(text)) is not None:
        minutes_string = pattern.findall(str(text))
        minutes_string = list(filter(None, minutes_string))

        try:
            if len(minutes_string[0][0]) > 0:
                return "Ilimitado"
            elif len(minutes_string[0][1]) > 0:
                return get_minutes_from_string(minutes_string[0][1])
        except IndexError:
            pass

        return minutes_string


def get_zero_rating(text):
    pattern = re.compile(r"(Facebook_WhatsApp_Twitter)")
    socialnetworks_string = pattern.findall(str(text))
    socialnetworks_string = list(filter(None, socialnetworks_string))
    return socialnetworks_string


def get_apps(text):
    pattern = re.compile(r"(Claro_Musica_y_Claro_Video)")
    apps_string = pattern.findall(str(text))
    apps_string = list(filter(None, apps_string))
    return apps_string


def get_life_span(text):

    def get_life_span_from_string(text):
        pattern = re.compile(r"([0-9]{0,9} d)")
        days = pattern.findall(str(text))
        days = list(filter(None, days))
        return days

    pattern = re.compile(r"([a-zA-Z0-9\s:]*Vigencia[a-zA-Z0-9\s:]*)")

    if pattern.findall(str(text)) is not None:
        life_span_string = pattern.findall(str(text))
        life_span_string = list(filter(None, life_span_string))
        print(life_span_string)
        try:
            if len(life_span_string) == 0:
                return "Ilimitado"
            elif len(life_span_string) > 0:
                return get_life_span_from_string(life_span_string)
        except IndexError:
            pass

        return life_span_string



