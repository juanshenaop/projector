import datetime
import urllib.request
import json
import csv
import re

from bs4 import BeautifulSoup
import utils as utils


"""Extracion de planes POSTPAGO"""

opener = urllib.request.build_opener()
opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
web = opener.open('https://compras.tigo.com.co/movil/pospago')


soup = BeautifulSoup(web.read(), "html.parser")
scripts = soup.find_all('div', {"class": ["banner"]})


with open('../data/tigoune_tarifas_postpaid.csv', 'w', newline='') as csvfile:
    csvfile = csv.writer(csvfile, delimiter='?')
    csvfile.writerow(["Fecha", "Operador", "Nombre", "Valor", "Minutos", "Datos", "SMS", "Redes Sociales", "APPs"])

    for script in scripts:
        print(script.div)
        print(script.div)
        fecha = datetime.date.today()
        csvfile.writerow([fecha, "TIGOUNE", "nombre", "precio", "EN DESARROLLO", "datos", "EN DESARROLLO", "EN DESARROLLO", "EN DESARROLLO"])
