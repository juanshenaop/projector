# -*- coding: utf-8 -*-
# Standard library imports
import datetime
import urllib.request
import json
import csv
import re

# Third party imports
from bs4 import BeautifulSoup

# Local application imports
import app.analytics.magnet_filters.utils_claro as utils




""" Extración de Paquetes - CLARO"""

""" Adquisición de Información """
opener = urllib.request.build_opener()
opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
web = opener.open('https://www.claro.com.co/personas/servicios/servicios-moviles/prepago/paquetes/')


""" Parser de Información """
soup = BeautifulSoup(web.read(), "html.parser")
scripts = soup.find_all('script')

pattern = re.compile(r"var jsonPlanes = jQuery.parseJSON(.*?)\n")


def get_plans():
    """ Recorre Planes """
    plans_list = []
    for script in scripts:
        if pattern.search(str(script.string)):
            data = pattern.findall(str(script.string))
            string_data = ''.join(data)
            string_json = string_data[2:-4]
            planes = json.loads(string_json)

            for plan in planes:
                "Empty List"
                atributes_list = []

                "EXTRACCIÓN DE CAMPOS"
                "FECHA"
                fecha = datetime.date.today()
                atributes_list.append(fecha)
                "NOMBRE"
                nombre = plan["fc_nombre"]
                atributes_list.append(nombre)
                "PRECIO"
                string_precio = plan["lstPlanCaracteristicas"][0]["fc_valor"]
                precio = plan["lstPlanPrecioDisp"][0]["ff_precio"]
                atributes_list.append(precio)
                "DATOS"
                string_datos = plan["lstPlanCaracteristicas"]
                datos = utils.get_data(plan)
                atributes_list.append(datos)
                "MINUTOS"
                minutos = (utils.get_minutes(plan))
                atributes_list.append(minutos)
                "SMS"
                sms = (utils.get_sms(plan))
                atributes_list.append(sms)
                "ZERO RATING"
                zero_rating = (utils.get_zero_rating(plan))
                atributes_list.append(zero_rating)
                "APPS"
                apps = (utils.get_apps(plan))
                atributes_list.append(apps)
                "VIGENCIA"
                vigencia = (utils.get_life_span(plan))
                atributes_list.append(vigencia)

                """List of Lists"""
                plans_list.append(atributes_list)

            return plans_list


#planes = get_plans()
