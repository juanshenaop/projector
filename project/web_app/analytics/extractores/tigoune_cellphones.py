# -*- coding: utf-8 -*-
# Standard library imports
import datetime
import urllib.request
import json
import csv
import re

# Third party imports
from bs4 import BeautifulSoup

# Local application imports
import app.analytics.magnet_filters.utils_tigo as utils


""" Extración de Celulares - TIGO-UNE """

""" Adquisición de Información """
opener = urllib.request.build_opener()
opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
web = opener.open('https://compras.tigo.com.co/celulares?O=OrderByPriceDESC&_ga=2.235212161.1040038227.1552358157-886495174.1552051770#1')

""" Parser de Información """
soup = BeautifulSoup(web.read(), "html.parser")

""" Obtiene Planes """
cellphones = soup.find_all('div', {"class": ["listItem"]})


def get_cellphones():
    """ Recorre Celulares """
    cellphones_list = []
    for idx, cellphone in enumerate(cellphones):
        "Empty List"
        atributes_list = []

        "EXTRACCIÓN DE CAMPOS"
        "FECHA"
        fecha = datetime.date.today()
        atributes_list.append(fecha)
        "NOMBRE"
        tag_nombre = cellphone.find_all('div', {"class": ["item-title"]})[0].text
        atributes_list.append(tag_nombre)
        "PRECIO"
        precio = cellphone.find_all('div', {"class": ["item-data"]})[0]
        precio_final = precio.find_all('div', {"class": ["best-price"]})[0].text
        atributes_list.append(precio_final)

        """List of Lists"""
        cellphones_list.append(atributes_list)

    return cellphones_list


#cellphones = get_cellphones()
