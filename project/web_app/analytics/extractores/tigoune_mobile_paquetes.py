# -*- coding: utf-8 -*-
# Standard library imports
import datetime
import urllib.request
import json
import csv
import re

# Third party imports
from bs4 import BeautifulSoup

# Local application imports
import app.analytics.magnet_filters.utils_tigo as utils

"""Extración de Paquetes - TIGO-UNE"""

""" Adquisición de Información """
opener = urllib.request.build_opener()
opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
web = opener.open('https://transacciones.tigo.com.co/servicios/paquetes?he=2')

""" Parser de Información """
soup = BeautifulSoup(web.read(), "html.parser")

""" Obtiene Planes """
planes = soup.find_all('li', {"data-tab": ["ofertas", "suscripciones"]})
description_planes = soup.find_all('', {"class": ["description_paquete_shop"]})
footer_planes = soup.find_all('', {"class": ["footer_paquete_shop"]})


def get_plans():
    """ Recorre Planes """
    plans_list = []
    for idx, plan in enumerate(planes):
        "Empty List"
        atributes_list = []

        "EXTRACCIÓN DE CAMPOS"
        "FECHA"
        fecha = datetime.date.today()
        atributes_list.append(fecha)
        "NOMBRE"
        nombre = plan.find_all('h3', {"class": ["tit_name"]})[0].string + " " + \
                 plan.find_all('h2', {"class": ["val_recursos"]})[0].string
        atributes_list.append(nombre)
        "PRECIO"
        precio = footer_planes[idx].find('h4', {"class": ["price"]}).string
        atributes_list.append(precio)
        "DATOS"
        datos = plan.find_all('h2', {"class": ["val_recursos"]})[0].string
        atributes_list.append(datos)
        "MINUTOS"
        minutos = utils.get_minutes(plan)
        atributes_list.append(minutos)
        "SMS"
        sms = "(SMS) En Desarrollo"
        atributes_list.append(sms)
        "ZERO RATING"
        zero_rating = "(Zero Rating) - En Desarrollo"
        atributes_list.append(zero_rating)
        "APPS"
        apps = "(APPS) - En Desarrollo"
        atributes_list.append(apps)
        "VIGENCIA"
        vigencia = utils.get_life_span(str(plan))
        atributes_list.append(vigencia)

        """List of Lists"""
        plans_list.append(atributes_list)

    return plans_list


#planes = get_plans()
