# -*- coding: utf-8 -*-
# Standard library imports
import datetime
import urllib.request
import json
import csv
import re

# Third party imports
from bs4 import BeautifulSoup

# Local application imports
import app.analytics.magnet_filters.utils_movistar as utils

"""Extración de Paquetes - MOVISTAR"""

""" Adquisición de Información """
opener = urllib.request.build_opener()
opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
web = opener.open('https://www.movistar.co/prepago')

""" Parser de Información """
soup = BeautifulSoup(web.read(), "html.parser")

""" Obtiene Planes """
planes = soup.find_all('div', {"class": ["c-plan", "c-plan--featured"]})


def get_plans():
    """ Recorre Planes """
    plans_list = []
    for plan in planes:
        "Empty List"
        atributes_list = []

        "EXTRACCIÓN DE CAMPOS"
        "FECHA"
        fecha = datetime.date.today()
        atributes_list.append(fecha)
        "NOMBRE"
        nombre = plan["data-gtm-name"]
        atributes_list.append(nombre)
        "PRECIO"
        precio = utils.get_price(plan)
        atributes_list.append(precio)
        "DATOS"
        datos = utils.get_data(plan)
        atributes_list.append(datos)
        "MINUTOS"
        caracteristicas = plan.find_all('div', {"class": ["c-plan__text"]})
        minutos = ((caracteristicas[0].string).lstrip()).rstrip()
        atributes_list.append(minutos)
        "SMS"
        sms = "SMS - Ilimitados"
        atributes_list.append(sms)
        "ZERO RATING"
        zero_rating = (utils.get_zero_rating(plan))
        atributes_list.append(zero_rating)
        "APPS"
        apps = "(APPS) - En Desarrollo"
        atributes_list.append(apps)
        "VIGENCIA"
        vigencia = utils.get_life_span((caracteristicas[1].string.lstrip()).rstrip())
        atributes_list.append(vigencia)

        """List of Lists"""
        plans_list.append(atributes_list)

    return plans_list


#planes = get_plans()
