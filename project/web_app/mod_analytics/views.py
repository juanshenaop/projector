# -*- coding: utf-8 -*-
# Standard library imports
import os
import glob

# Third party imports
from flask import Flask, g, Blueprint, render_template, flash, session, redirect, url_for, request, send_from_directory, \
    send_file, jsonify
from bokeh.core.properties import value
from bokeh.models import (HoverTool, FactorRange, Plot, LinearAxis, Grid, Range1d)
from bokeh.models.glyphs import VBar
from bokeh.plotting import Figure, show, output_file
from bokeh.embed import components
from bokeh.models.sources import ColumnDataSource
import numpy as np
import pandas as pd

# Local application imports
import project.web_app.analytics.comparator as comp

# Define the blueprint: 'start', set its url prefix: app.url/start
module_analytics = Blueprint('analytics', __name__, url_prefix='/analytics')


''' Definiciones de ANALYTICS'''


@module_analytics.route('/comparator', methods=['GET'])
def comparator():
    def tabs(item):
        if item is '\t':
            return False
        else:
            return True

    def spaces(item):
        if item is '\n':
            return False
        else:
            return True

    list_of_files = glob.glob("../project/documents/compared/*")
    latest_file = max(list_of_files, key=os.path.getctime)
    list_of_kb_files = glob.glob("../project/documents/knowledge_base/*")

    whole_base_paragraphs_list = []

    for kb_file in list_of_kb_files:
        base_paragraphs_list = comp.get_word_paragraphs(kb_file)
        whole_base_paragraphs_list.extend(base_paragraphs_list)


    base_document = list(filter(None, whole_base_paragraphs_list))

    query_paragraphs_list = comp.get_word_paragraphs(latest_file)
    query_document_one = list(filter(None, query_paragraphs_list))
    query_document_two = list(filter(tabs, query_document_one))
    query_document_three = list(filter(spaces, query_document_two))

    paragraphs = comp.compare_documents(base_document, query_document_three)
    return render_template('analytics/table.html', paragraphs=paragraphs)


#
#
#
# @module_start.route('/paquetes_movistar', methods=['GET'])
# def paquetes_movistar():
#     table = movistar_paquetes.get_plans()
#     return render_template('dashboard/paquetes.html', table=table, section_title="Paquetes Movistar")
#
#
# @module_start.route('/paquetes_tigo', methods=['GET'])
# def paquetes_tigo():
#     table = tigo_paquetes.get_plans()
#     return render_template('dashboard/paquetes.html', table=table, section_title="Paquetes TIGOUNE")
#
#
# @module_start.route('/pospago_claro', methods=['GET'])
# def pospago_claro():
#     table = claro_pospago.get_plans()
#     return render_template('dashboard/pospago.html', table=table, section_title="Pospago Claro")
#
#
# @module_start.route('/pospago_movistar', methods=['GET'])
# def pospago_movistar():
#     table = movistar_pospago.get_plans()
#     return render_template('dashboard/pospago.html', table=table, section_title="Pospago Movistar")
#
#
# @module_start.route('/celulares_tigo', methods=['GET'])
# def celulares_tigo():
#     table = tigo_celulares.get_cellphones()
#     return render_template('dashboard/celulares.html', table=table, section_title="Celulares TIGOUNE")
#
#
# @module_start.route('/download_data/<file>', methods=['GET'])
# def download_data(file):
#     try:
#         data_directory = os.path.abspath(os.path.join(__file__, "../../../Magnet/analytics/data"))
#         file_path = glob.glob(data_directory + '/**/' + '{}.csv'.format(file), recursive=True)
#         return send_file(file_path[0], attachment_filename="{}.csv".format(file), as_attachment=True)
#     except Exception as e:
#         return str(e)
#
#
# @module_start.route('/data/api/v1.0/<tipo>/<operador>', methods=['GET'])
# def get_data(tipo, operador):
#     if tipo == "planes" and operador == "claro":
#         list = claro_pospago.get_plans()
#         return jsonify(results=list)
#     elif tipo == "planes" and operador == "movistar":
#         list = movistar_pospago.get_plans()
#         return jsonify(results=list)
#     elif tipo == "paquetes" and operador == "claro":
#         list = claro_paquetes.get_plans()
#         return jsonify(results=list)
#     elif tipo == "paquetes" and operador == "movistar":
#         list = movistar_paquetes.get_plans()
#         return jsonify(results=list)
#     elif tipo == "paquetes" and operador == "tigo":
#         list = tigo_paquetes.get_plans()
#         return jsonify(results=list)
#     elif tipo == "celulares" and operador == "tigo":
#         list = tigo_celulares.get_cellphones()
#         return jsonify(results=list)
#     else:
#         return jsonify(results="Sin Resultados")
#
#
# @module_start.route('/dashboard', methods=['GET'])
# def get_dashboard():
#     # p = Figure(title='Sine', x_axis_type="datetime", plot_width=1110, plot_height=400)
#     # x = np.linspace(-10, 10, 200)
#     # y = np.sin(x)
#     # p.line(x=x, y=y)
#     # script, div = components(p)
#
#     operadores = ['Claro', 'Movistar', 'Tigo-Une']
#     categorias = ["Voz", "Datos", "Voz y Datos"]
#     colors = ["#c9d9d3", "#718dbf", "#e84d60"]
#
#     data = {'operadores': operadores,
#             'Voz': [2, 1, 4],
#             'Datos': [5, 3, 4],
#             'Voz y Datos': [3, 2, 4]}
#
#     p = Figure(x_range=operadores, plot_width=1400, plot_height=250, title="Tipos de Planes x Operador",
#                toolbar_location=None, tools="hover", tooltips="$name @operadores: @$name")
#
#     p.vbar_stack(categorias, x='operadores', width=0.9, color=colors, source=data,
#                  legend=[value(x) for x in categorias])
#
#     script, div = components(p)
#
#     return render_template('dashboard/dashboard.html', script=script, div=div, section_title="Dashboard")
