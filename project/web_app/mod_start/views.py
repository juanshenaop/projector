# -*- coding: utf-8 -*-
# Standard library imports

# Third party imports
from flask import Flask, g, Blueprint, redirect, url_for

# Local application imports

# Define the blueprint: 'start', set its url prefix: app.url/start
module_start = Blueprint('start', __name__, url_prefix='/')


''' Definiciones de START'''


# -- ENTRADA -- #
@module_start.route('/')
def start():
    return redirect(url_for('landing.index'))
