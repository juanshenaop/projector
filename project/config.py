# -*- coding: utf-8 -*-
import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

PRODUCTION_ENVIROMENT = False

# DATABASE
# Database connection.
# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'database/Happylives.db')
SQLALCHEMY_DATABASE_URI = ('postgres://postgres:postgres_p4ssw0rd@localhost:5432/postgres')
SQLALCHEMY_TRACK_MODIFICATIONS = False
DATABASE_CONNECT_OPTIONS = {}


# CSRF
# Enable protection agains - Cross-site Request Forgery (CSRF).
CSRF_ENABLED = True
# Use a secure, unique and absolutely secret key for signing the data.
CSRF_SESSION_KEY = 'Wm4y46Ve9kjxrlyOa1oMfVEyE36TGiRTq2jvmKRl'

# Secret key for signing cookies.
SECRET_KEY = os.environ.get('SECRET_KEY') or 'D426tYYDqykFhiRCaHmVmCq9h8ct3kWneydmT6AC'

# FOLDERS
# Folders to store data.
UPLOAD_DATA_FOLDER = './documents/compared'
UPLOAD_REPORTS_FOLDER = './files/reports'

# SALT
# Secret Password Salt BCript.
SECURITY_PASSWORD_SALT = '4juN0CbVeOdSWsw29BLFwzZnS8KxCOW3F3n0yY29'

# EMAIL
# Sending API
# SERVER_EMAIL = 'MAILGUN'
# API_KEY = 'fe5542552b25eaf62f80a0a82f0bfe6a-059e099e-27f83328'
# API_BASE_URL = 'https://api.mailgun.net/v3/send.happylives.co'

# Sending SMTP Email
SERVER_DIR = 'http://www.postdata.gov.co/'
SERVER_EMAIL = 'OUTLOOK'
SMTP_EMAIL = 'postdata@crcom.gov.co'
PASS_EMAIL = 'xN4zoogyWtLaE9'

# Secret for Email Sending
SECRET_CAPTCHA = "6LcCFWcUAAAAAB3XNKXh1uRDsE5DFZGL6F-4o-bq"
SITE_KEY_CAPTCHA = "6LcCFWcUAAAAAHhQoHtxO-EDNPF5-kM8XniPPUnu"
