# FLASK - GUNICORN - SUPERVISOR - PYTHON (NGINX)

FROM ubuntu:18.04
MAINTAINER Juan Henao <jshenaop@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update

# Python 3
RUN apt-get install -y python3 python3-pip python-virtualenv nginx supervisor
RUN pip3 install --upgrade pip
RUN pip3 install gunicorn

# Setup FLASK application
RUN mkdir -p /deploy/happylives_landing
COPY happylives_landing /deploy/happylives_landing
RUN pip3 install -r /deploy/happylives_landing/requirements_linux.txt

# Setup NGINX
#RUN rm /etc/nginx/sites-enabled/default
#COPY flask.conf /etc/nginx/sites-available/
#RUN ln -s /etc/nginx/sites-available/flask.conf /etc/nginx/sites-enabled/flask.conf
#RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Setup SUPERVISORD
RUN mkdir -p /var/log/supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY gunicorn.conf /etc/supervisor/conf.d/gunicorn.conf

# Ports
EXPOSE 5000

# Start processes
CMD ["/usr/bin/supervisord"]
