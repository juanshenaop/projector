@echo off 
setlocal EnableDelayedExpansion

SET var=%1

IF "%var%"=="" GOTO CASE1
IF NOT "%var%"=="" GOTO CASE2

:CASE1
SET /P envname="Enter Enviroment Name:"
ECHO Enviroment Name - %envname%
D:\Software\Python37x64\Scripts\virtualenv.exe --python D:\Software\Python37x64\python.exe %envname%

:CASE2
ECHO Creating Virtual Enviroment - %1
D:\Software\Python37x64\Scripts\virtualenv.exe --python D:\Software\Python37x64\python.exe %1
